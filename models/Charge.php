<?php namespace Qchsoft\Charges\Models;

use Model;
use Lovata\Toolbox\Traits\Helpers\PriceHelperTrait;

/**
 * Model
 */
class Charge extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use PriceHelperTrait;
    /**
     * @var string The database table used by the model.
     */
    public $table = 'qchsoft_charges_charge';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $fillable =[
        'title',
        'description',
        'price'
    ];

    
    public $arPriceField = ['price', 'old_price', 'discount_price'];

    protected $fSavedPrice = null;
    protected $fSavedOldPrice = null;
    protected $arSavedPriceList = [];
    protected $iActivePriceType = null;
    protected $sActiveCurrency = null;


    public $morphMany = [
        'price_link' => [
            Price::class,
            'name'       => 'item',
            'conditions' => 'price_type_id is NOT NULL',
        ],
    ];
    public $morphOne = [
        'main_price' => [
            Price::class,
            'name'       => 'item',
            'conditions' => 'price_type_id is NULL',
        ],
    ];
    
    /*
    
    $this->price = $obItem->setActivePriceType($iActivePriceType)->setActiveCurrency($sCurrencyCode)->price_value;
        $this->old_price = $obItem->setActivePriceType($iActivePriceType)->setActiveCurrency($sCurrencyCode)->old_price_value;
        $this->tax_percent = $obItem->tax_percent;
        $this->code = $obItem->code;
    
    */

    public function setActiveCurrency($sActiveCurrencyCode)
    {
        $this->sActiveCurrency = $sActiveCurrencyCode;

        return $this;
    }

    public function setActivePriceType($iPriceTypeID)
    {
        $this->iActivePriceType = $iPriceTypeID;

        return $this;
    }
}
