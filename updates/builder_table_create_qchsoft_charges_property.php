<?php namespace Qchsoft\Charges\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateQchsoftChargesProperty extends Migration
{
    public function up()
    {
        Schema::create('qchsoft_charges_property', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 150);
            $table->text('preview_text');
            $table->string('document', 50);
            $table->integer('active')->default(0);
            $table->string('slug', 250);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('qchsoft_charges_property');
    }
}
