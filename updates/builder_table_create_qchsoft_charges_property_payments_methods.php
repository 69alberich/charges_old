<?php namespace Qchsoft\Charges\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateQchsoftChargesPropertyPaymentsMethods extends Migration
{
    public function up()
    {
        Schema::create('qchsoft_charges_property_payments_methods', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('property_id');
            $table->integer('payment_method_id');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('qchsoft_charges_property_payments_methods');
    }
}
