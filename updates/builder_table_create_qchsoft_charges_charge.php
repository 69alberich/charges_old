<?php namespace Qchsoft\Charges\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateQchsoftChargesChanre extends Migration
{
    public function up()
    {
        Schema::create('qchsoft_charges_charge', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title', 150);
            $table->text('description');
            $table->decimal('price', 15, 2);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('qchsoft_charges_change');
    }
}
