<?php namespace Qchsoft\Charges\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateQchsoftChargesPaymentsStatus extends Migration
{
    public function up()
    {
        Schema::create('qchsoft_charges_payments_status', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 150);
            $table->text('description');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('qchsoft_charges_payments_status');
    }
}
