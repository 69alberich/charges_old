<?php namespace Qchsoft\Charges\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateQchsoftChargesProperty2 extends Migration
{
    public function up()
    {
        Schema::table('qchsoft_charges_property', function($table)
        {
            $table->string('zelle_email', 150);
           
        });
    }
    
    public function down()
    {
        Schema::table('qchsoft_charges_property', function($table)
        {
            $table->dropColumn('zelle_email');
        });
    }
}
