<?php namespace QchSoft\Charges\Classes\Event;

use Lovata\Toolbox\Classes\Event\ModelHandler;
use Lovata\OrdersShopaholic\Models\Order as OrderModel;
use Lovata\OrdersShopaholic\Models\OrderPosition as OrderPositionModel;
use Qchsoft\Charges\Models\Charge;
use Qchsoft\Charges\Models\Payment;
use Lovata\Buddies\Models\User;
use Carbon\Carbon;
class OrderModelHandler extends ModelHandler{

    public function subscribe($obEvent){
        
        OrderModel::extend(function($model) {

            if (!$model instanceof OrderModel) {
               
                return;
            }
            //pregunta si el modelo ha sido creado, si lo descomento no funciona en create 
            /*if (!$model->exists) {
              
                return;
            }
            //'detalles' => ['HesperiaPlugins\Hoteles\Models\DetalleReservacion', 'key' => 'reservacion_id'],

            /*'order_offer'           => [
                OrderPosition::class,
                'condition' => 'item_type = \Lovata\Shopaholic\Models\Offer',
            ],*/

            
            
            $model->hasMany["order_charge"] = [OrderPositionModel::class,
                'condition' => 'item_type = Qchsoft\Charges\Models\Charge',
            ];

            $model->hasMany["payments"]= [Payment::class];

            $model->belongsTo["manager"] = [
                User::class,
                'key'=> 'manager_id'
            ];
                        
            $model->addDynamicMethod('isAvailableToPay', function() use ($model) {
                
                $quantity = $model->property["vigency_value"];
                $format = $model->property["vigency_format"];
                
                $now = Carbon::now();
                if($format == 'hour'){
                    $length = $model->created_at->diffInHours($now);
                }elseif($format == 'day'){
                    $length = $model->created_at->diffInDays($now);
                }elseif ($format == 'week') {
                    $length = $model->created_at->diffInWeeks($now);
                }

                if ($length >  $quantity) {
                    return false;
                }else{
                    return true;
                }
            });
        
        });
    }

    /**
     * Get model class name
     * @return string
     */
    protected function getModelClass()
    {
        return Order::class;
    }

    /**
     * Get item class name
     * @return string
     */
    protected function getItemClass()
    {
        return OrderItem::class;
    }
}