<?php namespace Qchsoft\Charges;

use System\Classes\PluginBase;
use Event;


use Qchsoft\Charges\Classes\Event\OrdersControllerHandler;
use Qchsoft\Charges\Classes\Event\OrderPositionModelHandler;
use Qchsoft\Charges\Classes\Event\OrderModelHandler;
use Qchsoft\Charges\Classes\Event\UserModelHandler;
use Qchsoft\Charges\Classes\Event\BackendListsExtend;
//use Qchsoft\ShopPlus\Classes\Event\OrderModelHandler;


class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            'QchSoft\Charges\Components\CartSessionList' => 'CartSessionList',
            'QchSoft\Charges\Components\PaymentsHandler'   => 'PaymentsHandler',
            'QchSoft\Charges\Components\PaymentsList'   => 'PaymentsList',
        ];
    }

    public function registerSettings()
    {
    }
    
    public function boot(){
        $this->addEventListener();
    }



    protected function addEventListener(){
       
       Event::subscribe(OrdersControllerHandler::class);
       Event::subscribe(OrderPositionModelHandler::class);
       Event::subscribe(OrderModelHandler::class);
       Event::subscribe(UserModelHandler::class);
       Event::subscribe(BackendListsExtend::class);
    }

    public function registerSchedule($schedule)
    {
        $schedule->call(function () {

           $arOrders = \Lovata\OrdersShopaholic\Models\Order::where("status_id", 1)->get();
            //trace_log(count($arOrders));
            foreach ($arOrders as $obOrder) {
                if (isset($obOrder->property["vigency_value"]) && isset($obOrder->property["vigency_format"])) {
                    if(!$obOrder->isAvailableToPay()){
                        $obOrder->status_id = 5;
                        $obOrder->save();
                    }
                }else{
                    $obOrder->status_id = 5;
                    $obOrder->save();
                }
                
           }
           // \Db::table('recent_users')->delete();
        })->hourly();
    }
}
