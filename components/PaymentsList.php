<?php namespace QchSoft\Charges\Components;

use Cms\Classes\ComponentBase;
use Lovata\Toolbox\Classes\Helper\UserHelper;
use QchSoft\Charges\Models\Payment;
use Db;
class PaymentsList extends ComponentBase{

    protected $obUser;
    protected $obPaymentsList;

    public function componentDetails()
    {
        return [
            'name'        => 'Payments List',
            'description' => 'list of payments by user',
        ];
    }


    public function init(){

        $user = UserHelper::instance()->getUser();
        if(isset($user->properties[0])){
            $property =  $user->properties[0];
        $usersProperty = $property->users;
        $arUsersId = array();

        if(isset($user->properties[0])){
            foreach ($user->properties[0]->users as $userItem) {
              $id = $userItem->id;
              array_push($arUsersId, $id);
            }
            //dump($user->properties[0]->users);
        }
        $ordersIds = Db::table('lovata_orders_shopaholic_orders')->select('id')->whereIn("manager_id", $arUsersId)->get();
        $arOrdersId = array();
        //$this->obUser = UserHelper::instance()->getUser();
        foreach ($ordersIds as $orderItem) {
            $id = $orderItem->id;
            array_push($arOrdersId, $id);
          }
        
        //dump($ordersIds->toArray());
        $this->obPaymentsList = Payment::whereIn("order_id", $arOrdersId)->orderBy('id', 'desc')->get();
           
        }
        
    }


    public function getPayments(){
        return $this->obPaymentsList;
    }
}